package com.teste.s2;

public class BinaryTree {
	private int value;
	private BinaryTree left;
	private BinaryTree right;
	private int sumValues = 0;
	
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public BinaryTree getLeft() {
		return left;
	}
	public void setLeft(BinaryTree left) {
		this.left = left;
	}
	public BinaryTree getRight() {
		return right;
	}
	public void setRight(BinaryTree right) {
		this.right = right;
	}
	
	public void values(int value) {
    		child(value, left, right);
    }
	
	public void child(int value, BinaryTree leftTree, BinaryTree rightTree) {
		
		if (leftTree.getValue() == value) {
    		sumValues(leftTree);
    	} else if (rightTree.getValue() == value) {
    		sumValues(rightTree);
    	} else if (this.value == value) {
    		sumValues(this);
    	} else {
    		child(value, leftTree.getLeft(), rightTree.getRight());
    	}			
    	
	}
	
	/*
	 * Obs: O c�lculo est� considerando o valor do n� pai
	 * */
	public void sumValues(BinaryTree tree) {
		if (tree != null) {
			sumValues = sumValues + tree.getValue();
			sumValues(tree.getLeft());
			sumValues(tree.getRight());
		}
	}
	
	public int getSumValues() {
		return sumValues;
	}
	public void setSumValues(int sumValues) {
		this.sumValues = sumValues;
	} 
    
}

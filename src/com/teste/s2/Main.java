package com.teste.s2;

public class Main {

	public static void main(String[] args) {

		/* 1-A, 2-B, 3-C, 4-A, 5-A, 6-E, 7-E */
		
		/* Exerc�cio 8 */
		NumberUtils num = new NumberUtils();
		
		num.buildMagicNumber(10256, 512);
		
		System.out.println(num.getMagicNumber());
		
		
		/* Exerc�cio 9 */
		BinaryTree lefta = new BinaryTree();
		lefta.setValue(1);
		
		BinaryTree righta = new BinaryTree();
		righta.setValue(2);
		
		BinaryTree leftb = new BinaryTree();
		leftb.setValue(3);
		
		BinaryTree rightb = new BinaryTree();
		rightb.setValue(4);
		
		
		BinaryTree left1 = new BinaryTree();
		left1.setValue(5);
		left1.setLeft(lefta);
		left1.setRight(righta);
		
		BinaryTree right1 = new BinaryTree();
		right1.setValue(6);
		right1.setLeft(leftb);
		right1.setRight(rightb);

		
		BinaryTree main = new BinaryTree();
		
		main.setValue(0);
		main.setLeft(left1);
		main.setRight(right1);
		
		main.values(5);
		
		System.out.println(main.getSumValues());
	}

}

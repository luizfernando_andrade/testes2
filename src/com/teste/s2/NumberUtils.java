package com.teste.s2;

public class NumberUtils {
	
	private int index = 0;
	private Integer magicNumber = 0;
	private StringBuilder sb = new StringBuilder();
	
	public void buildMagicNumber(Integer a, Integer b) {
		
		if (index < a.toString().length()) {
			sb.append(a.toString().charAt(index)).toString();
		}
		
		if (index < b.toString().length()) {
			sb.append(b.toString().charAt(index)).toString();
		}
		
		if (index > b.toString().length() && index > a.toString().length()) {
			magicNumber = new Integer(sb.toString());
		} else {
			index++;
			buildMagicNumber(a, b);
		}
	}
	
	public Integer getMagicNumber() {
		
		if (magicNumber > 1000000) {
			return -1;
		}
		
		return magicNumber;
	}
}
